@import url(https://fonts.googleapis.com/css?family=Quicksand:400,700);
<div id="first-slider">
    <div id="carousel-example-generic" class="carousel slide carousel-fade">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            <li data-target="#carousel-example-generic" data-slide-to="3"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <!-- Item 1 -->
            <div class="item active slide1">
                <div class="row"><div class="container">

                        <div class="col-md-9 text-left">
                            <h3>Calorie<br>
                                ->215</h3>
                            <h4>Cooking complexity
                                <br>->Hard</h4>
                            <h4>Cooking time
                                <br>->30min max</h4>
                        </div>
                    </div></div>
            </div>
            <!-- Item 2 -->
            <div class="item slide2">
                <div class="row"><div class="container">
                        <div class="col-md-7 text-left">
                            <h3>Calorie<br>
                                ->65</h3>

                            <h4>Cooking complexity
                                <br>->Easy</h4>
                            <h4>Cooking time
                                <br>->25min max</h4>
                            </h4>

                        </div>


                    </div></div>
            </div>
            <!-- Item 3 -->
            <div class="item slide3">
                <div class="row"><div class="container">
                        <div class="col-md-7 text-left">
                            <h3>Calorie<br>
                                ->165</h3>
                            <h4>Cooking complexity
                                <br>->Medium</h4>
                            <h4>Cooking time
                                <br>->45min max</h4>
                        </div>

                    </div></div>
            </div>
            <!-- Item 4 -->
            <div class="item slide4">
                <div class="row"><div class="container">
                        <div class="col-md-7 text-left">
                            <h3>Calorie<br>
                                ->200</h3>
                            <h4>Cooking complexity
                                <br>->Easy</h4>
                            <h4>Cooking time
                                <br>->1 hr</h4>
                        </div>
                        <
                    </div></div>
            </div>
            <!-- End Item 4 -->


            <!-- End Wrapper for slides-->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <i class="fa fa-angle-left"></i><span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <i class="fa fa-angle-right"></i><span class="sr-only">Next</span>
            </a>
        </div>

        <span class="text-center">
<h4>Welcome! Here are recommendations for you.
</h4>
<h5><b>Personalized recommendations based on your tastes and cooks you're following</b></h5>
</span>

        <div class="container-fluid">

            <div class="row">

                @foreach($recipes as $recipe)
                    <div class="col-md-3">

                        <a href=""><img src={{asset('image/6a.jpg')}} width="300px" height="200px" alt="..." /></a>
                        <div class="caption post-content">

                            <h3>{{ $recipe['FirstName'] }}</h3>
                            <p>{{ $recipe['LastName'] }}</p>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star-empty"></span>
                        </div>
                    </div>
                @endforeach


                {{--<div class="col-md-3">--}}

                    {{--<a href="2.php"><img src={{asset('image/2.jpg')}} width="300px" height="200px" alt="..." /></a>--}}
                        {{--<div class="caption post-content">--}}

                            {{--<h3>Robots!</h3>--}}
                            {{--<p>Lorem ipsum dolor sit amet</p>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star-empty"></span>--}}
                        {{--</div>--}}
                {{--</div>--}}

                {{--<div class="col-md-3">--}}

                    {{--<a href="3.php"><img src="{{asset('image/3.jpg')}}" width="300px" height="200px" alt="..." /></a>--}}
                        {{--<div class="caption post-content">--}}

                            {{--<h3>Robots!</h3>--}}
                            {{--<p>Lorem ipsum dolor sit amet</p>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star-empty"></span>--}}
                        {{--</div>--}}
                {{--</div>--}}

                {{--<div class="col-md-3">--}}

                    {{--<a href="momo.php"><img src={{asset('image/momo.jpg')}} width="300px" height="200px" alt="..." /></a>--}}
                        {{--<div class="caption post-content">--}}

                            {{--<h3>Robots!</h3>--}}
                            {{--<p>Lorem ipsum dolor sit amet</p>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star-empty"></span>--}}
                        {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="row">--}}
                {{--<div class="col-md-3">--}}

                    {{--<a href="4.php"><img src={{asset('image/4.jpg')}} width="300px" height="200px" alt="..." /></a>--}}
                        {{--<div class="caption post-content">--}}

                            {{--<h3>Robots!</h3>--}}
                            {{--<p>Lorem ipsum dolor sit amet</p>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star-empty"></span>--}}
                        {{--</div>--}}
                {{--</div>--}}

                {{--<div class="col-md-3">--}}

                    {{--<a href="5.php"><img src={{asset('image/5.jpg')}} width="300px" height="200px" alt="..." /></a>--}}
                        {{--<div class="caption post-content">--}}

                            {{--<h3>Robots!</h3>--}}
                            {{--<p>Lorem ipsum dolor sit amet</p>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star-empty"></span>--}}
                        {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-3">--}}

                    {{--<a href="5a.php"><img src={{asset('image/5a.jpg')}} width="300px" height="200px" alt="..." /></a>--}}
                        {{--<div class="caption post-content">--}}

                            {{--<h3>Robots!</h3>--}}
                            {{--<p>Lorem ipsum dolor sit amet</p>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star-empty"></span>--}}
                        {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-3">--}}

                    {{--<a href="con1.php"><img src={{asset('image/con1.jpg')}} width="300px" height="200px" alt="..." /></a>--}}
                        {{--<div class="caption post-content">--}}

                            {{--<h3>Robots!</h3>--}}
                            {{--<p>Lorem ipsum dolor sit amet</p>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star-empty"></span>--}}
                        {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="row">--}}

                {{--<div class="col-md-3">--}}

                    {{--<a href="2a.php"><img src={{asset('image/2a.jpg')}} width="300px" height="200px" alt="..." /></a>--}}
                        {{--<div class="caption post-content">--}}

                            {{--<h3>Robots!</h3>--}}
                            {{--<p>Lorem ipsum dolor sit amet</p>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star-empty"></span>--}}
                        {{--</div>--}}
                {{--</div>--}}

                {{--<div class="col-md-3">--}}

                    {{--<a href="3a.php"><img src={{asset('image/3a.jpg')}} width="300px" height="200px" alt="..." /></a>--}}
                        {{--<div class="caption post-content">--}}

                            {{--<h3>Robots!</h3>--}}
                            {{--<p>Lorem ipsum dolor sit amet</p>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star-empty"></span>--}}
                        {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-3">--}}

                    {{--<a href="4a.php"><img src={{asset('image/4a.jpg')}} width="300px" height="200px" alt="..." /></a>--}}
                        {{--<div class="caption post-content">--}}

                            {{--<h3>Robots!</h3>--}}
                            {{--<p>Lorem ipsum dolor sit amet</p>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star-empty"></span>--}}
                        {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-3">--}}

                    {{--<a href="fish.php"><img src={{asset('image/fish.jpg')}} width="300px" height="200px" alt="..." /></a>--}}
                        {{--<div class="caption post-content">--}}

                            {{--<h3>Robots!</h3>--}}
                            {{--<p>Lorem ipsum dolor sit amet</p>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star"></span>--}}
                            {{--<span class="glyphicon glyphicon-star-empty"></span>--}}
                        {{--</div>--}}
                {{--</div>--}}
            </div>

        </div>
    </div>
</div>