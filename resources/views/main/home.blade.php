
<html>
<head>

@include('layout.declare')
</head>



<body>


<div class="container-fixed">

@include('layout.navbar')

</div>

<div>
    @include('main.content')
</div>

<div>
    @include('layout.footer')
</div>
</body>

</html>