
<html>
<head>
    @include('includes.headblade')
</head>

<body>

<div class="container">
<header class="row">
    @include('includes.header')

</header>
    @yield('content')
</div>



</body>
</html>