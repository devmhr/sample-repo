
    <meta charset="UTF-8">

    <!-- If IE use the latest rendering engine -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Set the page to the width of the device and set the zoon level -->
    <meta name="viewport" content="width = device-width, initial-scale = 1">
    <title>Bootstrap Tutorial</title>
    <link rel="stylesheet" href="{{asset('admin/ext.css')}}">
    <link rel="stylesheet" href="{{asset('admin/css/bootstrap.min.css')}}">


    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


    <!-- CUSTOM STYLE -->

    {{--<link rel="stylesheet" href="css/template-style.css">--}}
    {{--<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>--}}
    {{--<script type="text/javascript" src="js/jquery-ui.min.js"></script>--}}
    {{--<script type="text/javascript" src="js/modernizr.js"></script>--}}
    {{--<script type="text/javascript" src="js/responsee.js"></script>--}}

