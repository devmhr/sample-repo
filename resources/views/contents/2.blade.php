
<html>
<head>

    @include('layout.declare')
</head>



<body>


<div class="container-fixed">

    @include('layout.navbar')

</div>


<div class="container">
    <div class="row">
        <div class="col-md-6">
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star-empty"></span>


        </div>
        <div class="col-md-6">
            <style>

                .checkbox .cr,
                .radio .cr {

                . checkbox . cr . cr-icon,
                position: relative;
                    display: inline-block;
                    border: 1px solid #a9a9a9;
                    border-radius: .25em;
                    width: 53px;
                    height: 27px;
                    float: left;

                }
                .checkbox .cr .cr-icon,
                .radio .cr .cr-icon {
                    position: absolute;
                    font-size: 32px;
                    line-height: 0;
                    top: 50%;
                    left: 20%;
                    color:green;
                }

                .checkbox label input[type="checkbox"],
                .radio label input[type="radio"] {
                    display: none;
                }

                .checkbox label input[type="checkbox"] + .cr > .cr-icon,
                .radio label input[type="radio"] + .cr > .cr-icon {
                    transform: scale(7) rotateZ(0deg);
                    opacity: 0;
                    transition: all .3s ease-in;
                }

                .checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
                .radio label input[type="radio"]:checked + .cr > .cr-icon {
                    transform: scale(1) rotateZ(0deg);
                    opacity: 1;
                }

                .checkbox label input[type="checkbox"]:disabled + .cr,
                .radio label input[type="radio"]:disabled + .cr {
                    opacity: .5;
                }
            </style>


            <div class="checkbox">
                <label>
                    <input type="checkbox" value="">
                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                </label>
            </div>

            <img src="/public/image/2.jpg" width="550px" height="240px">

        </div>
    </div>

    <hr>
    <div class="row">
        <div class="col-sm-4">
            Ingredient<ul>
                <li>1 tea spoon</li>
                <li>1 tea spoon</li>
                <li>1 tea spoon</li>
                <li>1 tea spoon</li>
        </div>

        <div class="col-sm-4">
            <div class="spacer" style="height:20px;"></div>

            <li>1 tea spoon</li>
            <li>1 tea spoon</li>
            <li>1 tea spoon</li>
            <li>1 tea spoon</li>
            </ul>
        </div>
    </div>
    <div class="well">
        <u>Direction</u><span class="text pull-right">Add new note</span><span class="glyphicon glyphicon-pencil pull-right"></span>
        <ol type="1">
            <li><em>
                    Bring a large pot of salted water to a boil; cook linguine in boiling water until nearly tender, 6 to 8 minutes. Drain.</li>
            <li>
                Melt 2 tablespoons butter with 2 tablespoons olive oil in a large skillet over medium heat. Cook and stir shallots, garlic, and red pepper flakes in the hot butter and oil until shallots are translucent, 3 to 4 minutes. Season shrimp with kosher salt and black pepper; add to the skillet and cook until pink, stirring occasionally, 2 to 3 minutes. Remove shrimp from skillet and keep warm.</li>

            <li>
                Pour white wine and lemon juice into skillet and bring to a boil while scraping the browned bits of food off of the bottom of the skillet with a wooden spoon. Melt 2 tablespoons butter in skillet, stir 2 tablespoons olive oil into butter mixture, and bring to a simmer. Toss linguine, shrimp, and parsley in the butter mixture until coated; season with salt and black pepper. Drizzle with 1 teaspoon olive oil to serve.

            </li></em>
        </ol></div>

</div>


<div>

    @include('layout.footer')
</div>
</body>

</html>