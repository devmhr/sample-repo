<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//   return view('main.home');
//
//});

Route::get('/', ["uses" => "RecipeController@showHome", "as"=>"home"]);

Route::get('/about', function () {
    return view('new');

});

Route::get('/home', function () {
    return view('pages.home');

});

Route::get('/createProfile', function () {
    return view('pages.createProfile');

});

Route::get('/contactUs', function () {
    return view('pages.contactUs');

});

Route::get('/recipeDetail', function () {
    return view('pages.recipeDetail');

});

//project routes

