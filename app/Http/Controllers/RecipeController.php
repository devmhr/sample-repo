<?php

namespace App\Http\Controllers;

use App\UserInfo;
use Illuminate\Http\Request;

class RecipeController extends Controller
{
    //
    public function showHome(){

        $recipes = UserInfo::all();

//        print_r($results);die();

//        $recipes = [
//            ['title' => 'momo', 'desc' => 'this is description 1'],
//            ['title' => 'chowmin', 'desc' => 'this is description 2'],
//            ['title' => 'blabla', 'desc' => 'this is description 3']
//        ];
        return view('main.home')->with(['recipes' => $recipes]);
    }
}
